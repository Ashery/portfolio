<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="container">
                <time>Friday, 10 aug.</time>
                <span class="temperature">22°, living room</span>
        <nav>
            <ul>
                <li><a href="#">Stories</a></li>
                <li><a href="#">Schedule</a></li>
                <li><a href="#">Habitat</a></li>
                <li class="home"><a href="/">Home</a></li>
            </ul>
        </nav>
        
        </div>
        </header>

        <main>
            <div class="container">
                <div id="portfolio-slider">
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                    <div class="portfolio-item"></div>
                </div>
            </div>
        </main>

        <footer>
            <div class="container">
                <div itemscope itemtype="http://schema.org/Person">
                    <span class="country" itemprop="nationality">Denmark, Europe</span>
                    <img src="images/profile-picture.jpg" alt="A picture of myself" itemprop="image"/>
                    <h1 itemprop="name">Jesper Pedersen</h1>
                    <h2 itemprop="jobTitle">Developer, Project Coordinator</h2>

                    <div id="social-media">
                        <div class="social-media-item">
                            <a class="social-icon-linkedin" href="https://www.linkedin.com/in/jespernpedersen/" title="Se min Linkedin for flere informationer!"><img src="images/linkedin.svg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        



        <script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
    </body>
</html>